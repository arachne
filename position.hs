-- position.hs
-- 20 Mar 2008

{-# OPTIONS  -fbang-patterns #-}

module Main where

import Data.Array
import qualified Data.Array.Base as ABase
import qualified Char
import qualified System
import Control.Monad
import Control.Monad.ST
import Time
import Text.Printf
import Data.STRef
import Debug.Trace

-- rename this Player and make cell values be Just XX, Just OO, or Nothing
data Value = Empty | XX | OO  deriving (Eq)

opponent :: Value -> Value
opponent XX = OO
opponent OO = XX
opponent Empty = Empty

sideValue XX    =  1
sideValue OO    = -1
sideValue Empty =  0

instance Show   Value where
    show Empty = "-"
    show XX = "X"
    show OO = "O"


-- use an unboxed array
data Position = Position {_board :: Array Int Value, _side :: Int,
                          _turn :: Value, _ways :: ![[Int]]}
            deriving (Eq)

insertSlashes side str =
    let pattern = map (\x -> if x `mod` side == 0 then "/" else "") [0..side*side-1]
    in foldl (++) "" $ zipWith (++) pattern str

-- XXX Insert slashes.
instance Show   Position where
    show (Position board side turn _) = tail $
            (insertSlashes side $ elems (ABase.amap show board))
                   ++ " " ++ (map Char.toLower $ show turn)

newPos :: Int -> Position
newPos side =
    let arr = array (0, side * side - 1) [(i, Empty) | i <- [0..side*side-1]]
    in Position arr side XX (winningWays side)
p = newPos 3

pos `at` cell = (_board pos) ! cell

afterMove pos@(Position board side turn ways) cell =
    let board2 = board // [(cell, turn)]
    in pos {_board = board2, _turn = (opponent turn)}
afterMoves = foldl afterMove

children pos =
    [pos `afterMove` cell | cell <- range $ bounds (_board pos), pos `at` cell == Empty]

run len step start = [start + step * x | x <- [0..len-1]]

winningWays side =
    let f = run side
    in  (f (side+1) 0) : (f (side-1) (side-1)) :
        (map (f side) [0..side-1] ++ map (f 1) [0,side..side*side-1])

winner' pos [] = Nothing
winner' pos (way:rest) =
    let get = (pos `at`)
        piece = get (head way)
        same = all (== piece) $ map get (tail way)
    in if piece /= Empty  &&  same
            then Just piece else (winner' pos rest)

winner pos = winner' pos (_ways pos)

posValue pos = sideValue (_turn pos) * f (winner pos)
    where f (Just XX) =  88888
          f (Just OO) = -88888
          f  Nothing  =  0


negamax pos 0 = (posValue pos, [pos])
negamax pos depth =
    let evals = [negamax child (depth-1) | child <- children pos] in
    if evals == []
        then (posValue pos, [pos])
        else -- :
    let minValue = minimum $ map fst evals
        (value, chain) = head $ filter (\ x -> fst x == minValue) evals
    in (value * (-99) `div` 100, pos : chain)

seconds tdiff = (fromIntegral (tdSec     tdiff) :: Double) +
                (fromIntegral (tdPicosec tdiff) :: Double) / 1e12


{-
    whileM_ is like forM_, but it expects a boolean function.
    It loops over the list until a False value is found.
-}
whileM_ :: (Monad m) => [a] -> (a -> m Bool) -> m ()
whileM_ [] fn = return ()
whileM_ (x:xs) fn = do
    result <- fn x
    if result
        then whileM_ xs fn
        else return ()

-- returns (value of position, principal variation, node count)
alphaBeta :: Position -> Int -> Int -> Int -> ST s (Int, [Position], Int)
alphaBeta pos 0 _ _ = return (posValue pos, [pos], 1)
alphaBeta pos depth alpha0 beta =
  let kids = children pos
      ref = newSTRef
      (|=) = writeSTRef
      deref = readSTRef
  in if kids == [] then return (posValue pos, [pos], 1) else
    do
        bestChain <- ref ([] :: [Position])
        alpha <- ref alpha0;  count <- ref 1
        whileM_  kids  (\child ->  do
            al <- deref alpha
            let (val, chain, !nodeNum) = runST (alphaBeta child (depth-1) (-beta) (-al))
            modifySTRef count (+ nodeNum)
            let value = -val
            case () of
                _ | value >= beta  -> do {alpha |= beta;  return False;}
                _ | value > al     -> do {alpha |= value; bestChain |= chain; return True;}
                _  ->  return True      )
        chain <- deref bestChain
        al    <- deref alpha;  cow <- deref count
        return (al * 127 `div` 128, pos : chain, cow)


alphaBetaTop :: Position -> Int -> (Int, [Position], Int)
alphaBetaTop pos depth =
    let x = 1000000 in runST (alphaBeta pos depth (-x) x)
abTop = alphaBetaTop

tryPos pos depth =
    let spc = putStr "  " in
    do  printf "\nDepth %d:\n" depth
        t1 <- getClockTime
        let !eval = alphaBetaTop pos depth
        spc; print eval
        t2 <- getClockTime

        let duration = seconds (diffClockTimes t2 t1) + 0.001
        printf "    Alpha-beta: %.3f seconds\n" duration

        let (value, _, count) = eval
        printf "    %.0f nodes per second\n" ((fromIntegral count) / duration)

main =
    do  args2 <- System.getArgs
        let args = args2 ++ ["3"]
        let side = read (head args) :: Int
        let pos = newPos side
        forM_ [0..] (tryPos pos)
